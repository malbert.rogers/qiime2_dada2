#!/bin/bash

#SBATCH --mail-type=ALL
#SBATCH --mail-user=m.r.c.rogers-2@umcutrecht.nl
#SBATCH --mem=120G
#SBATCH -t 24:00:00                               ### Pipeline usually takes about 10-12 hours
#SBATCH -c 24 
#SBATCH -J qiime2_dada2_part1
#SBATCH -e qiime2_dada2_part1.error.log
#SBATCH -o qiime2_dada2_part1.output.log


### Input arguments: 1) project name 2) taxonomic reference database 3) Truncation position R1 4) Truncation position R2

print_usage() {
  printf "
Usage example: 
  qiime2_dada2_part1.sh --mapf mapping_example_project.txt --prj example_project --ref_db silva132 --trunc_f 275 --trunc_r 260

"
}


if [[ $# -eq 0 ]]; then
    print_usage
    exit 0
fi


while test $# -gt 0; do
    case "$1" in
	--mapf)
	    shift
	    mapf=$1
	    shift
	    ;;
	--prj)
            shift
	    prj=$1
            shift
            ;;
	--ref_db)
	    shift
	    ref_db=$1
	    shift
	    ;;
	--trunc_f)
	    shift
	    trunc_f=$1
	    shift
	    ;;
	--trunc_r)
	    shift
	    trunc_r=$1
	    shift
	    ;;
	*)
	    echo -e "\n INPUT ERROR: Unknown option $1!"
	    print_usage
	    exit 1;
	    ;;
    esac
done


echo -e "\n---QIIME 2 (includes DADA2) Pipeline started!---

Mapping file: $mapf
Project name: $prj
Taxonomic reference database: $ref_db
Truncation position forward reads: $trunc_f
Truncation position reverse reads: $trunc_r
"

### Validate mapping-file
#echo -e "===Validating mapping file...(step 1 of 7)\n"

#if [[ $mapf == *.txt ]] || [[ $mapf == *.tsv ]] || [[ $mapf == *.csv ]]; then
#    source activate /hpc/local/CentOS7/dla_mm/tools/miniconda3/envs/biocondaqiime
#    sed -i 's/ /_/g; s/\//./g; s/?/./g; s/!/./g; s/*/./g' $mapf
#    validate_mapping_file.py -m $mapf
#    source deactivate
#else
#    echo -e " ERROR! Mapping file format not supported. Please, make sure you've also included the extension (.txt, .tsv or .csv) of the mapping file's name.\n" 
#    exit 1
#fi

### Start Qiime2 conda environment
source activate /hpc/dla_mm/mrogers/miniconda3/envs/qiime2-2021.4
export LC_ALL=en_US.utf-8
export LANG=en_US.UTF-8
export TMPDIR="/hpc/tmp"


### Prepare sequencing read-files for Qiime2/DADA2 pipeline
echo -e "\n===Pre-processing sequencing read-files...(step 2 of 7)\n"

gunzip *R2*fastq.gz -c > R1.fastq
gunzip *R1*fastq.gz -c > R2.fastq
fastx_trimmer -i R1.fastq -f 1 -l 12 -Q 33 | fq_mergelines.pl > R1_barcode_temp
fastx_trimmer -i R2.fastq -f 1 -l 12 -Q 33 | fq_mergelines.pl > R2_barcode_temp
paste R1_barcode_temp R2_barcode_temp | awk -F "\t" '{print $5"\t"$2$6"\t"$3"\t"$4$8}' | fq_splitlines.pl > barcodes.fastq
seqtk trimfq -b 12 R1.fastq > forward.fastq
seqtk trimfq -b 12 R2.fastq > reverse.fastq

mkdir -p sequences
mv forward.fastq reverse.fastq barcodes.fastq -t sequences/
gzip -f sequences/*.fastq


### Demultiplex sequences
echo -e "===Demultiplexing sequencing reads...(step 3 of 7)\n"

qiime tools import --type EMPPairedEndSequences --input-path sequences/ --output-path ${prj}_pe-sequences.qza
qiime demux emp-paired --i-seqs ${prj}_pe-sequences.qza --m-barcodes-file *corrected.txt --m-barcodes-column BarcodeSequence --o-per-sample-sequences ${prj}_demux.qza --o-error-correction-details ${prj}_demux-details.qza --p-no-golay-error-correction --verbose
qiime demux summarize --i-data ${prj}_demux.qza --o-visualization ${prj}_demux.qzv
mkdir -p fastq_files_per_sample
qiime tools export --input-path ${prj}_demux.qza --output-path fastq_files_per_sample/


### Run DADA2 pipeline (--p-trim-left 21 = primer removal)
echo -e "\n===Running the Divisive Amplicon Denoising Algorithm version 2 (DADA2) pipeline...This might take a while...(step 4 of 7)\n" 

qiime dada2 denoise-paired --i-demultiplexed-seqs ${prj}_demux.qza --p-trim-left-f 21 --p-trim-left-r 21 --p-trunc-len-f $trunc_r --p-trunc-len-r $trunc_f --o-representative-sequences ${prj}_rep-seqs-dada2.qza --o-table ${prj}_table-dada2.qza --p-n-threads 24 --o-denoising-stats ${prj}_dada2-stats.qza --verbose
qiime metadata tabulate --m-input-file ${prj}_dada2-stats.qza --o-visualization ${prj}_dada2-stats.qzv
qiime feature-table summarize --i-table ${prj}_table-dada2.qza --o-visualization ${prj}_table-dada2.qzv --m-sample-metadata-file *corrected.txt
qiime feature-table tabulate-seqs --i-data ${prj}_rep-seqs-dada2.qza --o-visualization ${prj}_rep-seqs-dada2.qzv

if [[ $ref_db == "GREENGENES" ]] || [[ $ref_db == "GG" ]] || [[ $ref_db == "gg" ]] || [[ $ref_db == "Green"* ]] || [[ $ref_db == "green"* ]] || [[ $ref_db == "13_8" ]]; then
    qiime feature-classifier classify-sklearn --i-classifier /hpc/local/CentOS7/dla_mm/tools/qiime2/gg-13-8-99-nb-classifier0.24.1.qza --i-reads ${prj}_rep-seqs-dada2.qza --o-classification ${prj}_taxonomy.qza --verbose
elif [[ $ref_db == "SILVA-132" ]] || [[ $ref_db == "silva132" ]] || [[ $ref_db == "132" ]] || [[ $ref_db == "SILVA132" ]] || [[ $ref_db == "Silva132" ]] || [[ $ref_db == "Silva-132" ]] || [[ $ref_db == "silva-132" ]]; then
    qiime feature-classifier classify-sklearn --i-classifier /hpc/local/CentOS7/dla_mm/tools/qiime2/silva-132-99-nb-classifier0.21.2.qza --i-reads ${prj}_rep-seqs-dada2.qza --o-classification ${prj}_taxonomy.qza --verbose
elif [[ $ref_db == "SILVA138" ]] || [[ $ref_db == "silva-138" ]] || [[ $ref_db == "SILVA-138" ]] || [[ $ref_db == "silva138" ]] || [[ $ref_db == "Silva138" ]] || [[ $ref_db == "138" ]] || [[ $ref_db == "Silva-138" ]] || [[ $ref_db == "Silva" ]] || [[ $ref_db == "silva" ]] || [[ $ref_db == "SILVA" ]]; then
    qiime feature-classifier classify-sklearn --i-classifier /hpc/local/CentOS7/dla_mm/tools/qiime2/silva-138-99-nb-classifier0.24.1.qza --i-reads ${prj}_rep-seqs-dada2.qza --o-classification ${prj}_taxonomy.qza --verbose
else
    echo -e " ERROR! Unknown database choice. Please, choose and fill in either Greengenes and Silva for the option --ref_db.\n" 
    exit 1
fi

qiime metadata tabulate --m-input-file ${prj}_taxonomy.qza --o-visualization ${prj}_taxonomy.qzv

### Generate and export phylogeny tree
echo -e "\n===Generating Generating and exporting phylogeny tree...(step 5 of 8)\n"

qiime phylogeny align-to-tree-mafft-fasttree --i-sequences ${prj}_rep-seqs-dada2.qza --o-alignment ${prj}_aligned-rep-seqs.qza --o-masked-alignment ${prj}_masked-aligned-rep-seqs.qza --o-tree ${prj}_unrooted-tree.qza --o-rooted-tree ${prj}_rooted-tree.qza
qiime tools export --input-path ${prj}_unrooted-tree.qza --output-path exported-unrooted-tree
qiime tools export --input-path ${prj}_rooted-tree.qza --output-path exported-rooted-tree

### Generate bar-plots
echo -e "\n===Generating a taxa barplot visualization file...(step 6 of 8)\n"

qiime taxa barplot --i-table ${prj}_table-dada2.qza --i-taxonomy ${prj}_taxonomy.qza --m-metadata-file *corrected.txt --o-visualization ${prj}_taxa-bar-plots.qzv 


### Export and convert feature-tables
echo -e "\n===Exporting feature tables (.tsv and .biom formats)...(step 7 of 8)\n"

mkdir -p exported_table

for i in 1 2 3 4 5 6 7; do qiime taxa collapse --i-table ${prj}_table-dada2.qza --i-taxonomy ${prj}_taxonomy.qza --p-level $i --o-collapsed-table ${prj}_table-dada2-L${i}.qza; done
for i in 1 2 3 4 5 6 7; do qiime tools export --input-path ${prj}_table-dada2-L${i}.qza --output-path exported_table && mv exported_table/feature-table.biom exported_table/feature-table_L${i}.biom && biom convert -i exported_table/feature-table_L${i}.biom -o ${prj}_table-dada2-L${i}.tsv --to-tsv; echo Exported table L${i}; done 
qiime tools export --input-path ${prj}_table-dada2.qza --output-path exported_table
mv exported_table/feature-table.biom exported_table/feature-table_L8.biom
biom convert -i exported_table/feature-table_L8.biom -o temp-L8.tsv --to-tsv
qiime tools export --input-path ${prj}_taxonomy.qza --output-path exported_table
head -2 temp-L8.tsv > ${prj}_table-dada2-L8.tsv
tail -n +2 exported_table/taxonomy.tsv | sort -k1 | cut -f2 | paste -d '\t', <(tail -n +3 temp-L8.tsv | sort -k1) - >> ${prj}_table-dada2-L8.tsv

echo "Exported table L8"


### Group output-files
mkdir -p features_tables_biom/
mkdir -p features_tables_tsv/
mkdir -p visualization_files/
mkdir -p tree_files/

mv exported_table/*.biom features_tables_biom/
mv *table-dada2-L*.tsv features_tables_tsv/
mv exported_table/taxonomy.tsv features_tables_tsv/
cp *corrected.txt features_tables_tsv/
mv *.qzv visualization_files/
mv exported-unrooted-tree/tree.nwk tree_files/unrooted-tree.nwk
mv exported-rooted-tree/tree.nwk tree_files/rooted-tree.nwk

### Remove temporary files
echo -e "\n===Removing unnecessary temporary files...(step 8 of 8)\n"

rm -r overlib.js R2.fastq R1.fastq R2_barcode_temp R1_barcode_temp sequences/ temp-L8.tsv exported_table/ exported-unrooted-tree/ exported-rooted-tree/


### Create raw log file
mkdir -p logs/
cp /hpc/local/CentOS7/dla_mm/bin/qiime2_dada2_part1.sh logs/raw_code_part1.log
cp qiime2_dada2_part1.error.log logs/qiime2_dada2_part1_errors.log
cp qiime2_dada2_part1.output.log logs/qiime2_dada2_part1_outputs.log

echo -e "\n\n------

Specified parameters:

Mapping file: $mapf 
Project name: $prj
Taxonomic reference database: $ref_db         ### --i-classifier                                                         
Truncation position forward reads: $trunc_f   ### --p-trunc-len-f                   
Truncation position reverse reads: $trunc_r   ### --p-trunc-len-r

 
" >> logs/raw_code_part1.log

echo $(date) >> logs/raw_code_part1.log

echo -e "---QIIME 2 pipeline (part 1) completed!---

If you're interested in more expanded statistical results, including core (alpha- and beta-) diversity analyses, please proceed to part 2 of the pipeline.


## Malbert Rogers, June 2018. Latest update: June 14th, 2021 ##\n"                  


## Greengenes: Released in August 2013 --> Data (sequences and taxa) retrieved in June 2012 (?)
## SILVA-132: Released in December 2017 --> Data (sequences and taxa) retrieved in July 2017
## SILVA-138: Released in December 2019 --> Data (sequences and taxa) retrieved in November 2018
