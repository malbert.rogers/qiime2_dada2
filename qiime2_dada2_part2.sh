#!/bin/bash

#SBATCH --mail-type=ALL
#SBATCH --mail-user=m.r.c.rogers-2@umcutrecht.nl
#SBATCH --mem=30G
#SBATCH -t 4:00:00                              ### Pipeline usually takes between 1 and 2 hours
#SBATCH -J qiime2_dada2_part2
#SBATCH -e qiime2_dada2_part2.error.log
#SBATCH -o qiime2_dada2_part2.output.log


### Input arguments: 1) project name 2) cut-off value

print_usage() {
  printf "                                                                                                                                                                                                  
Usage example:                                                                                                                                                                                              
  qiime2_dada2_part2.sh --prj example_project --co 10000                                                                                 
                                                                                                                                                                                                           
"
}


if [[ $# -eq 0 ]] ; then
    print_usage
    exit 0
fi


while test $# -gt 0; do
    case "$1" in
	--prj)
            shift
            prj=$1
            shift
            ;;
	--co)
            shift
            co=$1
            shift
            ;;
	*)
            echo -e "\n INPUT ERROR: Unknow option $1!"
            print_usage
            exit 1;
	    ;;
    esac
done


echo -e "\n---QIIME 2 core diversity analyses pipeline started!---

Project name:  $prj
Cut-off value: $co

"

### Start Qiime2 conda environment 
source activate /hpc/dla_mm/mrogers/miniconda3/envs/qiime2-2021.4
export LC_ALL=en_US.utf-8
export LANG=en_US.UTF-8
export TMPDIR="/hpc/tmp"


### Generate phylogenetic tree
echo -e "===Generating phylogenetic tree...(step 1 of 2)\n"

qiime alignment mafft --i-sequences ${prj}_rep-seqs-dada2.qza --o-alignment ${prj}_aligned-rep-seqs.qza 
qiime alignment mask --i-alignment ${prj}_aligned-rep-seqs.qza --o-masked-alignment ${prj}_masked-aligned-rep-seqs.qza 
qiime phylogeny fasttree --i-alignment ${prj}_masked-aligned-rep-seqs.qza --o-tree ${prj}_unrooted-tree.qza 
qiime phylogeny midpoint-root --i-tree ${prj}_unrooted-tree.qza --o-rooted-tree ${prj}_rooted-tree.qza


### Performe Alpha and beta diversity analyses
echo -e "\n===Performing alpha-diversity analyses, and exporting visualization files...(step 2 of 3)\n"

qiime diversity core-metrics-phylogenetic --i-phylogeny ${prj}_rooted-tree.qza --i-table ${prj}_table-dada2.qza --p-sampling-depth $co --m-metadata-file *corrected.txt --output-dir ${prj}_core-metrics-results-${co} 
qiime diversity alpha-group-significance  --i-alpha-diversity ${prj}_core-metrics-results-${co}/faith_pd_vector.qza --m-metadata-file *corrected.txt --o-visualization ${prj}_core-metrics-results-${co}/faith-pd-group-significance.qzv
qiime diversity alpha-group-significance --i-alpha-diversity ${prj}_core-metrics-results-${co}/evenness_vector.qza --m-metadata-file *corrected.txt --o-visualization ${prj}_core-metrics-results-${co}/evenness-group-significance.qzv
qiime diversity alpha-group-significance --i-alpha-diversity ${prj}_core-metrics-results-${co}/shannon_vector.qza --m-metadata-file *corrected.txt --o-visualization ${prj}_core-metrics-results-${co}/shannon-group-significance.qzv

echo -e "\n===Performing beta-diversity analyses, and exporting visualization files...(step 3 of 3)\n"

for catg in $(head -1 *corrected.txt | tr '\t' '\n' | grep -v 'SampleID\|Barcode\|Description\|Linker' | tr '\n' ' '); do \
qiime diversity beta-group-significance --i-distance-matrix ${prj}_core-metrics-results-${co}/weighted_unifrac_distance_matrix.qza --m-metadata-file *corrected.txt --o-visualization ${prj}_core-metrics-results-${co}/${catg}_weighted-unifrac-significance.qzv --m-metadata-column $catg; echo "Analyses performed for column '$catg'"; done

echo -e ""

for catg in $(head -1 *corrected.txt | tr '\t' '\n' | grep -v 'SampleID\|Barcode\|Description\|Linker' | tr '\n' ' '); do \
qiime diversity beta-group-significance --i-distance-matrix ${prj}_core-metrics-results-${co}/unweighted_unifrac_distance_matrix.qza --m-metadata-file *corrected.txt --o-visualization ${prj}_core-metrics-results-${co}/${catg}_unweighted-unifrac-significance.qzv --m-metadata-column $catg; echo "Analyses performed for column '$catg'"; done

qiime emperor plot --i-pcoa ${prj}_core-metrics-results-${co}/unweighted_unifrac_pcoa_results.qza --m-metadata-file *corrected.txt --o-visualization ${prj}_core-metrics-results-${co}/unweighted-unifrac-emperor.qzv --p-ignore-missing-samples
qiime emperor plot --i-pcoa ${prj}_core-metrics-results-${co}/bray_curtis_pcoa_results.qza --m-metadata-file *corrected.txt --o-visualization ${prj}_core-metrics-results-${co}/bray-curtis-emperor.qzv --p-ignore-missing-samples
qiime diversity alpha-rarefaction --i-table ${prj}_table-dada2.qza --i-phylogeny ${prj}_rooted-tree.qza --p-max-depth ${co} --m-metadata-file *corrected.txt --o-visualization ${prj}_core-metrics-results-${co}/${prj}_alpha-rarefaction.qzv

mkdir ${prj}_core-metrics-results-${co}/visualization_files
mkdir ${prj}_core-metrics-results-${co}/artifact_files

mv ${prj}_core-metrics-results-${co}/*.qzv ${prj}_core-metrics-results-${co}/visualization_files/
mv ${prj}_core-metrics-results-${co}/*.qza ${prj}_core-metrics-results-${co}/artifact_files/


### log raw code
cp /hpc/local/CentOS7/dla_mm/bin/qiime2_dada2_part1.sh logs/raw_code_part2.log

echo -e "\n\n------

Specified parameters:

Project name:  $prj
Cut-off value: $co


" >> logs/raw_code_part2.log

echo $(date) >> logs/raw_code_part2.log

echo -e "\n---QIIME 2 core diversity analyses completed!--- 


## Malbert Rogers, June 2018. Latest update: June 14th, 2020 ##\n"
