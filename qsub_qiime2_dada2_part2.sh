#!/bin/bash


print_usage_min() {
  printf "
INPUT ERROR: Missing arguments. Please check the usage example below to see what option(s) you are missing!

Usage example:
  qsub_qiime2_dada2_part2.sh --prj example_project --co 10000

For a more extensive explanation of the different options, try qsub_qiime2_dada2_part2.sh --help.

"
}


print_usage_max() {
  printf "
INPUT ERROR: Too many arguments. Please check the usage example below to see what option(s) are accepted!

Usage example:
  qsub_qiime2_dada2_part2.sh --prj example_project --co 10000

For a more extensive explanation of the different options, try qsub_qiime2_dada2_part2.sh --help.

"
}


print_usage() {
  printf "
Usage example: 
  qsub_qiime2_dada2_part2.sh --prj example_project --co 10000 

Required files (need to be in the same directory where the pipeline will be called):
  *) All required files should have been generated after running the 'part 1' of the pipeline. 
 

Required inputs:
  --prj       Same name/prefix for the project used for part 1 of the pipeline (qsub_qiime2_dada2_part1.sh).
  --co        Cut-off/rarefaction/sampling-depth value for QIIME 2 core diversity analyses. Samples with a total amount of sequences lower than this value will be excluded from most core diversity analyses (has to be a number). Recommended: >10k for fecal samples, >6k for oral samples. 
 
General option:
  --help      Show this help message.

"
}


if [[ $1 == "--help" ]]; then
    print_usage
    exit 0
elif [ $# -gt 0 -a $# -lt 4 ]; then
    print_usage_min
    exit 1
elif [ $# -gt 4 ]; then
    print_usage_max
    exit 1
elif [ $# -eq 0 ]; then
    print_usage
    exit 0
else
    qsub /hpc/local/CentOS7/dla_mm/bin/qiime2_dada2_part2.sh ${1} ${2} ${3} ${4}
fi
