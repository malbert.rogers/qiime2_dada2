#!/bin/bash


print_usage_min() {
  printf "
INPUT ERROR: Missing arguments. Please check the usage example below to see what option(s) you are missing!

Usage example:
  qsub_qiime2_dada2_part1.sh --mapf mapping_example_project.txt --prj example_project --ref_db silva --trunc_f 275 --trunc_r 260

For a more extensive explanation of the different options, try qsub_qiime2_dada2_part1.sh --help.

"
}


print_usage_max() {
  printf "
INPUT ERROR: Too many arguments. Please check the usage example below to see what option(s) are accepted!

Usage example:
  qsub_qiime2_dada2_part1.sh --mapf mapping_example_project.txt --prj example_project --ref_db silva --trunc_f 275 --trunc_r 260

For a more extensive explanation of the different options, try qsub_qiime2_dada2_part1.sh --help.

"
}


print_usage() {
  printf "
Usage example: 
  qsub_qiime2_dada2_part1.sh --mapf mapping_example_project.txt --prj example_project --ref_db silva --trunc_f 275 --trunc_r 260 

Required files (need to be in the same directory where the pipeline will be called):
  1) MiSeq 2x300 paired-end sequencing read-files.
  2) A QIIME 2 compatible mapping file. You can find an example here: /hpc/local/CentOS7/dla_mm/tools/qiime2/mapping_example_project.txt

Required inputs:
  --mapf      Specify the mapping file. 
  --prj       A name/prefix for the project.
  --ref_db    Taxonomic reference database to use, GreenGenes (13_8) or Silva (132).
  --trunc_f   Truncation position for the forward (R1) reads (has to be a number). Recommended choice when uncertain: 275.
  --trunc_r   Truncation position for the reverse (R2) reads (has to be a number). Recommended choice when uncertain: 260.
     
General option:
  --help      Show this help message.

"
}


if [[ $1 == "--help" ]]; then
    print_usage
    exit 0
elif [ $# -gt 0 -a $# -lt 10 ]; then
    print_usage_min
    exit 1
elif [ $# -gt 10 ]; then
    print_usage_max
    exit 1
elif [ $# -eq 0 ]; then
    print_usage
    exit 0
else
    qsub /hpc/local/CentOS7/dla_mm/bin/qiime2_dada2_part1.sh ${1} ${2} ${3} ${4} ${5} ${6} ${7} ${8} ${9} ${10}
fi
