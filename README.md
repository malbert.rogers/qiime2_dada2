# QIIME 2 (16S) PIPELINE USING DADA2


#### To run the pipeline **qiime2_dada2_part1** (aim = obtaining features (SVs) tables):

1. Make sure you are in the correct directory containing the raw MiSeq gzipped read-files (R1 and R2) and a mapping file (has to be .txt, no spaces allowed in the filenames)
2. Fill in a project name (with no spaces), followed by the name of the mapping file

====

**Example:**

Project is called *example_project* and its corresponding mapping file is *example_mapping.txt*. 

On the HPC, the command to run the script would have to be: 
```
qsub_qiime2_dada2_part1.sh example_project example_mapping.txt
```
====

The script will be submitted as a job to run on one of the HPC nodes. If you want to run the pipeline interactively (See How-To on HPC-wiki), use the script qiime2_dada2_part1.sh (without "qsub" in front).


<br />
**After running part 1, features-tables at different taxonomy levels are obtained. If a core diversity analysis is also desired, continue on with part 2.** 

Check the project_name_table-dada2.qzv to choose the subsampling depth for part 2 of the pipeline.  
<br />



#### To run the pipeline **qiime2_daad2_part2** (aim = Core Diversity Analyses):
                                                                                                                                                                   
1. Make sure you are in the correct directory containing the outputs of part1 of the pipeline
2. Fill in the same project name used for part 1, followed by a numeric value for the subsampling depth

====

**Example:**

Project is called *example_project* and the chosen subsampling depth is *10000*. 

On the HPC, the command to run the pipeline would have to be: 
```
qsub_qiime2_dada2_part2.sh example_project 10000  
```
====


#### **IMPORTANT!** This pipeline was built specifically for Fadrosh-like prepared 16S samples ran on the MiSeq (see paper 'Fadrosh et al., 2014' for more info)!
